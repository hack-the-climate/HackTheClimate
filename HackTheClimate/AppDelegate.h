//
//  AppDelegate.h
//  HackTheClimate
//
//  Created by Maynard Tong on 9/4/15.
//  Copyright (c) 2015 Maynard Tong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

