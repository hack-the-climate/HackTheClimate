//
//  HTCHTTPSessionManager.m
//  HackTheClimate
//
//  Created by Maynard Tong on 9/5/15.
//  Copyright (c) 2015 Maynard Tong. All rights reserved.
//

#import "HTCHTTPSessionManager.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation HTCHTTPSessionManager

+ (instancetype)sharedClient
{
    static dispatch_once_t onceToken;
    static AFHTTPSessionManager *client;
    
    dispatch_once(&onceToken, ^{
        client = [[HTCHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://10.104.161.240:1337"] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        client.requestSerializer = [AFJSONRequestSerializer serializer];
        client.responseSerializer = [AFJSONResponseSerializer serializer];
    });
    
    return client;
}
- (void)addElectricMeterValue:(NSInteger)value
{
    NSDictionary *parameters = @{@"value":@(value)};
    
    NSString *userId = [FBSDKAccessToken currentAccessToken].userID;
    NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    
    [self.requestSerializer setValue:userId forHTTPHeaderField:@"fbId"];
    [self.requestSerializer setValue:accessToken forHTTPHeaderField:@"accessToken"];

    [self POST:@"v1/electricMeters" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

@end
