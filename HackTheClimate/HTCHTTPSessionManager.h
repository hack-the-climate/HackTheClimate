//
//  HTCHTTPSessionManager.h
//  HackTheClimate
//
//  Created by Maynard Tong on 9/5/15.
//  Copyright (c) 2015 Maynard Tong. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface HTCHTTPSessionManager : AFHTTPSessionManager

+ (instancetype)sharedClient;
- (void)addElectricMeterValue:(NSInteger)value;

@end
