//
//  HTCOCRViewController.m
//  HackTheClimate
//
//  Created by Maynard Tong on 9/4/15.
//  Copyright (c) 2015 Maynard Tong. All rights reserved.
//

#import "HTCOCRViewController.h"
#import <FBSDKCoreKit/FBSDKButton.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TesseractOCR.h>
#import "HTCHTTPSessionManager.h"

@interface HTCOCRViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, FBSDKLoginButtonDelegate>

@property (nonatomic, strong) UIButton *startMonitoringButton;
@property (nonatomic, strong) NSTimer *monitoringTimer;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;

@property (nonatomic, strong) NSOperationQueue *teserractOperationQueue;

@end

@implementation HTCOCRViewController

- (void)loadView
{
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.backgroundColor = [UIColor blackColor];
    
    _startMonitoringButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _startMonitoringButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _startMonitoringButton.layer.borderWidth = 1.0f;
    [_startMonitoringButton setTitle:@"Monitor Electric Meter" forState:UIControlStateNormal];
    [_startMonitoringButton setTintColor:[UIColor blueColor]];
    [_startMonitoringButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_startMonitoringButton addTarget:self action:@selector(didTapStartMonitoring:) forControlEvents:UIControlEventTouchUpInside];
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    [loginButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    loginButton.delegate = self;
    [loginButton setCenter:self.view.center];
    [self.view addSubview:loginButton];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    if (!self) {
        return nil;
    }

    self.teserractOperationQueue = [[NSOperationQueue alloc] init];
    self.teserractOperationQueue.maxConcurrentOperationCount = 1;
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    if (!error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showCameraView];
        });
    }
}
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}

- (void)showCameraView
{
    self.imagePickerController = [[UIImagePickerController alloc] init];
    self.imagePickerController.delegate = self;
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePickerController.showsCameraControls = NO;
    self.imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    self.imagePickerController.navigationBarHidden = YES;
    self.imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    
    [self.imagePickerController.view addSubview:self.startMonitoringButton];
    
    [self.imagePickerController.view addConstraint:[NSLayoutConstraint constraintWithItem:_startMonitoringButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_startMonitoringButton.superview attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.imagePickerController.view addConstraint:[NSLayoutConstraint constraintWithItem:_startMonitoringButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_startMonitoringButton.superview attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-20.0f]];
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)didTapStartMonitoring:(id)sender
{
    [self.imagePickerController takePicture];
    self.monitoringTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(processCurrentMeterReadings:) userInfo:nil repeats:YES];
    [self.startMonitoringButton removeTarget:self action:@selector(didTapStartMonitoring:) forControlEvents:UIControlEventTouchUpInside];
    [self.startMonitoringButton setTitle:@"Stop Monitoring" forState:UIControlStateNormal];
    [self.startMonitoringButton addTarget:self action:@selector(didTapStopMonitoring:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didTapStopMonitoring:(id)sender
{
    [_startMonitoringButton setTitle:@"Monitor Electric Meter" forState:UIControlStateNormal];
    [self.startMonitoringButton removeTarget:self action:@selector(didTapStopMonitoring:) forControlEvents:UIControlEventTouchUpInside];
    [_startMonitoringButton addTarget:self action:@selector(didTapStartMonitoring:) forControlEvents:UIControlEventTouchUpInside];
    [self.monitoringTimer invalidate];

}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selectedPhoto = info[UIImagePickerControllerOriginalImage];
    UIImage *scaledImage = [self scaleImage:selectedPhoto maxDimension:640];
    
    [self performImageRecognition:scaledImage];
}

- (void)processCurrentMeterReadings:(id)sender
{
    [self.imagePickerController takePicture];
}

- (UIImage *)scaleImage:(UIImage *)image maxDimension:(CGFloat)maxDimension
{
    CGSize scaledSize = CGSizeMake(maxDimension, maxDimension);
    CGFloat scaledFactor;
    
    if (image.size.width > image.size.height) {
        scaledFactor = image.size.height / image.size.width;
        scaledSize.width = maxDimension;
        scaledSize.height = scaledSize.width * scaledFactor;
    }
    else {
        scaledFactor = image.size.width / image.size.height;
        scaledSize.height = maxDimension;
        scaledSize.width = scaledSize.height * scaledFactor;
    }
    
    UIGraphicsBeginImageContext(scaledSize);
    [image drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

- (void)performImageRecognition:(UIImage *)image
{
    G8RecognitionOperation *operation = [[G8RecognitionOperation alloc] initWithLanguage:@"eng"];
    
    operation.tesseract.charWhitelist = @"0123456789";
    operation.tesseract.image = [image g8_blackAndWhite];

    operation.recognitionCompleteBlock = ^(G8Tesseract *recognizedTesseract) {
        // Retrieve the recognized text upon completion
        NSLog(@"%@", [recognizedTesseract recognizedText]);
        NSString *value = [recognizedTesseract recognizedText];
        if ([value integerValue] == 0) {
            return;
        }
        [[HTCHTTPSessionManager sharedClient] addElectricMeterValue:[value integerValue]];
    };
    
    [self.teserractOperationQueue addOperation:operation];
}



@end
